### **DevOps Repo Ansible Playbooks for Providing Examples How to Interact with WIndows Server** ###
###
###
### 
### Requirements for Running Playbook on Windows 10
#### Make sure you have Git for Windows Install If you are using Windows. Located Here https://git-scm.com/download/win
#### If you are using Windows 10 make sure you have the Linux Subsystem Installled
#### Instructions for Installing Windows 10 Linux Subsystem. Located Here https://docs.microsoft.com/en-us/windows/wsl/install-win10
#### Install Ansible inside the Windows Linux Subsystem. Follow the Ubuntu Instructions for Installing Ansible in the Windows 10 Linux Subsystem
#### http://docs.ansible.com/ansible/latest/intro_installation.html#latest-releases-via-apt-ubuntu

## ** Instruction of Usage **
#### 1. Open the the Linux Subsystem Bash Terminal 
#### 2. Create a folder for code -  _mkdir playbook_code_
#### 3. cd to folder -  _cd playbook_code_
#### 4. Clone Playbook from BitBucket
#### ** git clone https://wilsoncharlesr@bitbucket.org/wilsoncharlesr/devops_win_utilities.git **
#### 5. Then you can run the Playbook
#### Run the Playbook from the Playbook Folder
#### ansible-playbook -vvvv win_app.yml -check --ask-vault-pass